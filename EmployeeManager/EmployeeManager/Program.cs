﻿using EmployeeManager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManager
{
    class Program
    {
        static void Main(string[] args)
        {
            // List of employees
            List<Employee> employees = new List<Employee>();

            Console.Write("Enter the number of employees: ");
            int numberEmployees = int.Parse(Console.ReadLine());

            for (int i = 0; i < numberEmployees; i++)
            {
                Console.WriteLine("Employee #{0}", i + 1);
                Console.Write("Outsourced (y/n)? "); // outsourced = terceirizado
                string awnser = Console.ReadLine();

                Console.Write("Name: ");
                string name = Console.ReadLine();
                Console.Write("Hours: ");
                int hours = int.Parse(Console.ReadLine());
                Console.Write("Value per hour: ");
                double valuePerHour = double.Parse(Console.ReadLine());

                if (awnser == "y")
                {
                    Console.Write("Additional charge: ");
                    double additionalCharge = double.Parse(Console.ReadLine());

                    // create an instance of an object of type OutsourcedEmployee
                    OutsourcedEmployee employee = new OutsourcedEmployee(name, hours, valuePerHour, additionalCharge);
                    employees.Add(employee);
                }

                else
                {
                    // create an instance of an object of type Employee
                    Employee employee = new Employee(name, hours, valuePerHour);
                    employees.Add(employee);
                }
            }

            Console.WriteLine();
            Console.WriteLine("PAYMENTS");
            for (int i = 0; i < employees.Count(); i++)
            {
                Console.WriteLine("{0} - $ {1}", employees[i].Name, employees[i].Payment());
            }
        }
    }
}
